package com.applying.demo.spring.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
// 2. register the springSecurityFilterChain Filter for every URL in your application
public class SecurityWebApplicationInitializer 
  extends AbstractSecurityWebApplicationInitializer {

}