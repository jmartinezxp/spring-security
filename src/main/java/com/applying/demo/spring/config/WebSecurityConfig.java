package com.applying.demo.spring.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.applying.demo.spring.controller.CustomAuthenticationSuccessHandler;
/**
 * 1.
 * Spring Security Java Configuration
 * creates a Servlet Filter known as the springSecurityFilterChain which is responsible for all the security
 *
 * Require authentication to every URL in your application
 * Generate a login form for you
 * Allow the user with the Username user and the Password password to authenticate with form based authentication
 * Allow the user to logout
 */
@EnableWebSecurity
public class WebSecurityConfig  extends WebSecurityConfigurerAdapter{

	  /*
    @Bean
    public InMemoryUserDetailsManager inMemoryUserDetailsManager() {
    	UserBuilder users = User.withDefaultPasswordEncoder();
    	InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
    	manager.createUser(users.username("user").password("password").authorities("PERM_SOLICITUD_READ","PERM_SOLICITUD_WRITE","PERM_SOLICITUD_DELETE","PERM_SOLICITUD_UPDATE","PERM_SOLICITUD_SEARCH").build());
    	manager.createUser(users.username("jose").password("password").authorities("PERM_SOLICITUD_READ","PERM_SOLICITUD_WRITE","PERM_SOLICITUD_SEARCH").build());
    	manager.createUser(users.username("audit").password("password").authorities("PERM_SOLICITUD_SEARCH","PERM_SOLICITUD_READ").build());
    	return manager;
    }	
	   @Override
	    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	        auth.userDetailsService(inMemoryUserDetailsManager());
	    }
	   */
	/*
	  @Autowired
	  private DataSource dataSource;
	   @Override
	   protected void configure(AuthenticationManagerBuilder auth) throws Exception {

	     auth.jdbcAuthentication().dataSource(dataSource)
	         .usersByUsernameQuery("select username, password, enabled"
	             + " from users where username=?")
	         .authoritiesByUsernameQuery("select username, authority "
	             + "from authorities where username=?")
	         .passwordEncoder(new BCryptPasswordEncoder());
	   }	   
	  */ 	 
		
	/*
	   @Bean
	   public UserDetailsService userDetailsService() {
	     return new UserDetailsServiceImp();
	   };
	   
	   @Bean
	   public BCryptPasswordEncoder passwordEncoder() {
	     return new BCryptPasswordEncoder();
	   };
	   
	   @Override
	   protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	     auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder());
	   }
	  */
	
    @Autowired
    private CustomAuthenticationProvider authProvider;
 
    @Override
    protected void configure(
      AuthenticationManagerBuilder auth) throws Exception {
  
        auth.authenticationProvider(authProvider);
    }
    
	   @Autowired
	   CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;
	   /**/
	   @Override
	   protected void configure(HttpSecurity http) throws Exception {
		 
//	     http.authorizeRequests().anyRequest().hasAnyRole("ADMIN", "USER")
//	     .and()
//	     .httpBasic(); // Authenticate users with HTTP basic authentication
	     
	     
	     http
			.authorizeRequests()
			.antMatchers("/vistaSolicitud").access("hasAuthority('PERM_SOLICITUD_READ')")
			.antMatchers("/eliminarSolicitud/*").access("hasAuthority('PERM_SOLICITUD_DELETE')")
			.antMatchers("/agregarSolicitud").access("hasAuthority('PERM_SOLICITUD_WRITE')")
			.antMatchers("/modificarSolicitud").access("hasAuthority('PERM_SOLICITUD_UPDATE')")
			.antMatchers("/vistaSolicitudes").access("hasAuthority('PERM_SOLICITUD_SEARCH')")
				.anyRequest().authenticated()
				.and()
			.formLogin()
		      .loginPage("/login") // Specifies the login page URL
		      .loginProcessingUrl("/signin") // Specifies the URL,which is used 
		      .usernameParameter("userid")  // Username parameter, used in name attribute
		      .passwordParameter("passwd")  // Password parameter, used in name attribute
		      .successHandler(customAuthenticationSuccessHandler)
		      /*
		      .successHandler((req,res,auth)->{    //Success handler invoked after successful authentication
		         for (GrantedAuthority authority : auth.getAuthorities()) {
		            System.out.println(authority.getAuthority());
		         }
		         System.out.println(auth.getName());
		         res.sendRedirect("/vistaSolicitudes"); // Redirect user to index/home page
		      })*/
//		    .defaultSuccessUrl("/")   // URL, where user will go after authenticating successfully.
		                                // Skipped if successHandler() is used.
		      .failureHandler((req,res,exp)->{  // Failure handler invoked after authentication failure
		         String errMsg="";
		         if(exp.getClass().isAssignableFrom(BadCredentialsException.class)){
		            errMsg="Invalid username or password.";
		         }else{
		            errMsg="Unknown error - "+exp.getMessage();
		         }
		         req.getSession().setAttribute("message", errMsg);
		         res.sendRedirect("/login"); // Redirect user to login page with error message.
		      })
//		    .failureUrl("/login?error")   // URL, where user will go after authentication failure.
		                                    //  Skipped if failureHandler() is used.
		      .permitAll() // Allow access to any URL associate to formLogin()
		      .and()
		      
		      
		      .logout()
		      .logoutUrl("/signout")   // Specifies the logout URL, default URL is '/logout'
		      .logoutSuccessHandler((req,res,auth)->{   // Logout handler called after successful logout 
		         req.getSession().setAttribute("message", "You are logged out successfully.");
		         res.sendRedirect("/login"); // Redirect user to login page with message.
		      })
//		    .logoutSuccessUrl("/login") // URL, where user will be redirect after successful
		                                  //  logout. Ignored if logoutSuccessHandler() is used.
		      .permitAll() // Allow access to any URL associate to logout() 
		      .and()
		      .csrf().disable(); // Disable CSRF support
	     
	   }
	   
		/**/
	    
	    
	    	   
}
