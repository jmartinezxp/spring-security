package com.applying.demo.spring.config;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.applying.demo.spring.bean.User;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider{
    @Override
    public Authentication authenticate(Authentication authentication) 
      throws AuthenticationException {
  
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();
         
        User objUser = shouldAuthenticateAgainstThirdPartySystem(name,password); 
        if (objUser!=null) {
	        Collection<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
	        for(String authority : objUser.getRoles()){
		        authorities.add(new SimpleGrantedAuthority(authority));
	        } 
            return new UsernamePasswordAuthenticationToken(name, password, authorities);
        } else {
            return null;
        }
    }
 
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(
          UsernamePasswordAuthenticationToken.class);
    }
    
    public User shouldAuthenticateAgainstThirdPartySystem( String username , String password ){
    	if(username.equalsIgnoreCase("user")) {
  	      return new User(username, "password", "PERM_SOLICITUD_READ","PERM_SOLICITUD_WRITE","PERM_SOLICITUD_DELETE","PERM_SOLICITUD_UPDATE","PERM_SOLICITUD_SEARCH");
  	    }else if(username.equalsIgnoreCase("jose")) {
  		      return new User(username, "password", "PERM_SOLICITUD_READ","PERM_SOLICITUD_WRITE","PERM_SOLICITUD_SEARCH");
  		}else if(username.equalsIgnoreCase("audit")) {
  		      return new User(username, "password","PERM_SOLICITUD_SEARCH","PERM_SOLICITUD_READ");
  		}
    	return null;
    }
}
