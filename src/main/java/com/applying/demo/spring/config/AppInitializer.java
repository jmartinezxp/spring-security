package com.applying.demo.spring.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.applying.demo.spring.controller.CustomAuthenticationSuccessHandler;

public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

   @Override
   protected Class<?>[] getRootConfigClasses() {
	   //3.loaded in our existing ApplicationInitializer. 
      return new Class[] {CustomAuthenticationProvider.class, WebSecurityConfig.class ,CustomAuthenticationSuccessHandler.class};
   }

   @Override
   protected Class<?>[] getServletConfigClasses() {
      return new Class[] { WebMvcConfig.class };
   }

   @Override
   protected String[] getServletMappings() {
      return new String[] { "/" };
   }
}
